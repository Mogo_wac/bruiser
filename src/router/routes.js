import Champions from '../views/Champions/champions';
import Builds from '../views/Builds/builds';
import ClientError from '../views/ClientError/clientError';
import Patch from "../views/Patch/patch";
import Stream from "../views/Stream/stream";
import Live from "../views/Live/live";

const routes = [
    {path: "/champions", component: () => { return <Champions page="champions" />}},
    {path: "/builds", component: Builds},
    {path: "/patch", component: Patch},
    {path: "/stream", component: Stream},
    {path: "/live", component: Live},
    {component: ClientError}
]

export {routes};