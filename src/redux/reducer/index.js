import { combineReducers } from 'redux';

import NavbarReducer from './navbarReducer';

export default combineReducers ({
    NavbarReducer,
})