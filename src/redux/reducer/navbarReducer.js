const initialState = {
    user: {},
}

const NavbarReducer = (state = {initialState}, action) => {
    let newState;
    switch(action.type){
        case "GET_USER":
            newState = {
                ...state,
                user: action.value
            }
            return newState;
        default:
            return state;
    }
}

export default NavbarReducer;