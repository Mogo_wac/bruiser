import React, { Component } from "react"
import { socket } from "../../Socket/socket";
import { Carousel} from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./stream.scss";
import {
    UserOutlined
} from '@ant-design/icons';
import { TwitchEmbed, TwitchPlayer, TwitchChat } from 'react-twitch-embed';
import {Drawer} from "antd";

export default class Stream extends Component {
    constructor(props) {
        super(props);
        this.state = {
            streams: [],
            stream: "",
            visible: false
        }
    }

    componentDidMount() {

        socket.emit("getStreams");
        socket.on("getStreamsResult", (res) => {
            if (!res) {
                return false
            }
            else {
                this.setState({ streams: res.streams })
            }
        })
    }

    showSideBar = (i) => {
        console.log(i);
        
        this.setState({stream : i})
        this.setState({ visible: true });
    }
    onClose = () => {
        this.setState({ visible: false });
    };

    render() {
        console.log(this.state.streams);

        // console.log(this.state.streams.data && this.state.streams.data.map(element=>element));
        let formatData = this.state.streams.data && this.state.streams.data.map(element => {
            let split = element.thumbnail_url.split("{")
            element.new_thumbnail = split[0] + "1980x1080.jpg";
        })

        return (
            <div id="live">

                <Carousel className="live-slider">
                    {
                        this.state.streams.data && this.state.streams.data.map((element, i) => {
                            return (
                                <div key={i} className="img-container" >
                                    <div className="img-overlay" onClick={e => this.showSideBar(element.user_name)}>
                                        {/* <a href={"https://www.twitch.tv/" + element.user_name} target="_blank" className="img-redirect"/> */}

                                        <div className="img-title"><span>{element.title}</span></div>
                                        <div className="img-viewer"><UserOutlined className="img-viewer-icon" />{element.viewer_count} spectateurs</div>
                                        <div className="img-username">@{element.user_name}</div>
                                        <div className="img-live"><p>{element.type}</p></div>
                                    </div>
                                    <img src={element.new_thumbnail} className="carousel-img" />
                                </div>
                            );
                        })
                    }
                </Carousel>
                <Drawer 
                    title={this.state.stream}
                    placement="right"
                    onClose={e => this.onClose()}
                    visible={this.state.visible}
                    closable={true}
                    width="80%"
                    className="stream-live"
                    style={{fontSize: "21px", color:"white"}}
                >
                    <TwitchEmbed
                        className="streamPlayer"
                        theme="dark"
                        channel={this.state.stream}
                        width={"100%"}
                        height={"680px"}
                    />
                </Drawer>
            </div>
        )
    }
}
                // <Drawer title="Live" placement="right" onClose={this.onClose()} visible={this.state.visible}>
                //     {/* <TwitchEmbed channel="otplol_" theme="dark"/> */}
                //     <h1>test</h1>
                // </Drawer>