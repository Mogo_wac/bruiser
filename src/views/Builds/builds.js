import React , {Component} from 'react';
import "./builds.scss";
import ReactTable from 'react-table-6';
import {
    Button,
    Drawer, 
    Tabs,
    Select,
    Form,
    Input
} from 'antd';
import {
    HeartOutlined
} from '@ant-design/icons'
import { socket } from '../../Socket/socket';
import {connect} from 'react-redux';


const { TabPane } = Tabs;
const { Option } = Select;


class Builds extends Component{
    constructor(props){
        super(props);
        this.state = {
            visible: false,
            champions: [],
            items: [],
            response: "",
            builds: [],
            visibleBuild: false,
            buildInfo: []
        }
    }

    componentDidMount(){
        socket.emit("getChampions");
        socket.on("getChampionsResult", (res)=>{
            this.setState({champions: res.response.data});
        });

        socket.emit("getItems");
        socket.on("getItemsResult", (res)=>{
            // console.log("res:", res)
            this.setState({items: res.response.data});
        });

        socket.emit("getBuilds");
        socket.on("getBuildsResult", (res)=>{
            // console.log("res:", res)
            this.setState({builds: res.response.builds});
        });
    }
    componentDidUpdate(prevState, prevProps){

        console.log(prevProps.builds.length, this.state.builds.length);
        
        console.log(prevProps.builds != this.state.builds);

    }

    showDrawer = (value) => {
        this.setState({visible: !value})
      };
    onClose = () => {
        this.setState({visible: false});
    };

    onFinish = (values) => {
        console.log("Succes: ", values);
        const{
            build_name,
            champions,
            item_1,
            item_2,
            item_3,
            item_4,
            item_5,
            item_6,
        } = values;
        if (!this.props.userData) {
            return false;
        }
        const email = this.props.userData.email;
        
        const build = {
            build_name,
            champions,
            item_1,
            item_2,
            item_3,
            item_4,
            item_5,
            item_6,
            email
        }

        socket.emit("createBuild", build);
        socket.on("createBuildResult", (res)=>{
            if (res) {
                this.setState({response: res.response});
                let output = document.getElementById("tabs-output-build");
                output.style.opacity = 1;
                output.style.color = "lightgreen"
                setTimeout(function(){
                    output.style.opacity = 0
                }, 3500);
                console.log(res);
            }
            else{
                this.setState({response: res.error});
                let output = document.getElementById("tabs-output-build");
                output.style.opacity = 1;
                output.style.color = "red"
                setTimeout(function(){
                    output.style.opacity = 0
                }, 3500);
            }
            return false;
            
        })
        
        
    }

    onFinishFailed = (errorInfo) => {
        console.log("Failed: ",errorInfo);
        
    };

    onRowClick = (state, rowInfo, column, instance) => {
        let bool= false;
        return {
            onClick: (e, handleOriginal) => {
                    console.log(rowInfo.original);
                    this.setState({buildInfo: rowInfo.original})
                    this.setState({visibleBuild: !bool})
                }
        }
    }
    
    openBuild = (value) =>{
        return this.setState({visibleBuild: !value});
    }

    sendLike = (value) =>{
        console.log("clicked", value);
        
        socket.emit("sendLike", value);
        socket.on("sendLikeResult", res =>{
            console.log(res);
        })
    }
    
    render(){
        console.log(this.state.builds);
        
        const columns=[
            {
                Header: "Champion",
                accessor: "champion",
                Cell: props => {
                    return <div><img src={props.value} alt="champ icon"/></div>
                }
            },
            {
                Header: "1er Item",
                accessor: "item_1",
                Cell: props => {
                    return <div><img src={props.value} alt="item icon"/></div>
                }
            },
            {
                Header: "2nd Item",
                accessor: "item_2",
                Cell: props => {
                    return <div><img src={props.value} alt="item icon"/></div>
                }
            },
            {
                Header: "3ème Item",
                accessor: "item_3",
                Cell: props => {
                    return <div><img src={props.value} alt="item icon"/></div>
                }
            },
            {
                Header: "4ème Item",
                accessor: "item_4",
                Cell: props => {
                    return <div><img src={props.value} alt="item icon"/></div>
                }
            },
            {
                Header: "5ème Item",
                accessor: "item_5",
                Cell: props => {
                    return <div><img src={props.value} alt="item icon"/></div>
                }
            },
            {
                Header: "6ème Item",
                accessor: "item_6",
                Cell: props => {
                    return <div><img src={props.value} alt="item icon"/></div>
                }
            },
            {
                Header: "Créer par",
                accessor: "email_build_owner",
                Cell: props => {
                    return <div>{props.value}</div>
                }
            }
        ]
        let newData = [];
        newData.push(Object.entries(this.state.champions));
        let formatData = newData[0].map(element => element[1]);

        let newItems = [];
        newItems.push(Object.entries(this.state.items));
        let formatItems = newItems[0].map(element => element[1]);


        console.log(this.props);
        return(
            <div
                id="builds"
            >
                {
                    this.props.userData && this.props.userData ?
                <ul id="builds-menu">
                    <li>
                        <Button
                            onClick={() => this.showDrawer(this.state.visible)}
                            >
                            Créer un build
                        </Button>
                    </li>
                </ul>
                : ""
                }
                <ReactTable 
                    data={this.state.builds}
                    columns={columns}
                    getTrProps={this.onRowClick}
                /> 
                <Drawer
                    title={"Build"}
                    placement="right"
                    width={"50%"}
                    closable={true}
                    onClose={this.onClose}
                    visible={this.state.visible}
                >
                    <Tabs>
                        <TabPane tab="Créer un build" key="1">
                            <Form
                                name="basic"
                                initialValues={{ remember: true }}
                                onFinish={this.onFinish}
                                onFinishFailed={this.onFinishFailed}
                            >
                                <Form.Item
                                    label="Nom du build"
                                    name="build_name"
                                    rules={[{ required: true, message: "Veuillez ajouter un nom a votre build." }]}
                                >
                                    <Input placeholder="Nom du build" style={{width: "200px"}} />
                                </Form.Item>
                                <Form.Item
                                    label="Champions"
                                    name="champions"
                                    rules={[{ required: true, message: "Veuillez sélectionner un champion." }]}
                                >
                                    <Select
                                        placeholder="Champions" 
                                        style={{width: "200px"}} 
                                        bordered={false}
                                    >
                                        {
                                            formatData.map((element,i) => {
                                                    return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/champion/${element.image.full}`}>
                                                                <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/champion/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                                {element.name}
                                                            </Option>
                                            })
                                        }

                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label="1er Item"
                                    name="item_1"
                                    rules={[{ required: true, message: "Veuillez sélectionner un item." }]}
                                >
                                    <Select
                                        placeholder="Items" 
                                        style={{width: "200px"}} 
                                        bordered={false}
                                    >
                                        {
                                            formatItems.map((element,i) => {
                                                    return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`}>
                                                                <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                                {element.name}
                                                            </Option>
                                            })
                                        }

                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label="2nd Item"
                                    name="item_2"
                                    rules={[{ required: true, message: "Veuillez sélectionner un item." }]}
                                >
                                    <Select
                                        placeholder="Items" 
                                        style={{width: "200px"}} 
                                        bordered={false}
                                    >
                                        {
                                            formatItems.map((element,i) => {
                                                    return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`}>
                                                                <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                                {element.name}
                                                            </Option>
                                            })
                                        }

                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label="3ème Item"
                                    name="item_3"
                                    rules={[{ required: true, message: "Veuillez sélectionner un item." }]}
                                >
                                    <Select
                                        placeholder="Items" 
                                        style={{width: "200px"}} 
                                        bordered={false}
                                    >
                                        {
                                            formatItems.map((element,i) => {
                                                    return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`}>
                                                                <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                                {element.name}
                                                            </Option>
                                            })
                                        }

                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label="4ème Item"
                                    name="item_4"
                                    rules={[{ required: true, message: "Veuillez sélectionner un item." }]}
                                >
                                    <Select
                                        placeholder="Items" 
                                        style={{width: "200px"}} 
                                        bordered={false}
                                    >
                                        {
                                            formatItems.map((element,i) => {
                                                    return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`}>
                                                                <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                                {element.name}
                                                            </Option>
                                            })
                                        }

                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label="5ème Item"
                                    name="item_5"
                                    rules={[{ required: true, message: "Veuillez sélectionner un item." }]}
                                >
                                    <Select
                                        placeholder="Items" 
                                        style={{width: "200px"}} 
                                        bordered={false}
                                    >
                                        {
                                            formatItems.map((element,i) => {
                                                    return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`}>
                                                                <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                                {element.name}
                                                            </Option>
                                            })
                                        }

                                    </Select>
                                </Form.Item>
                                <Form.Item
                                    label="6ème Item"
                                    name="item_6"
                                    rules={[{ required: true, message: "Veuillez sélectionner un item." }]}
                                >
                                    <Select
                                        placeholder="Items" 
                                        style={{width: "200px"}} 
                                        bordered={false}
                                    >
                                        {
                                            formatItems.map((element,i) => {
                                                    return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`}>
                                                                <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/item/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                                {element.name}
                                                            </Option>
                                            })
                                        }

                                    </Select>
                                </Form.Item>
                                <Form.Item>
                                    <Button type="primary" htmlType="submit">
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                            <div id="tabs-output-build"></div>
                        </TabPane>
                        <TabPane tab="Editer un build" key="2">
                            <Select >

                            </Select>
                        </TabPane>
                        <TabPane tab="Supprimer un build" key="3">
                            <Select >

                            </Select>
                        </TabPane>
                    </Tabs>
                </Drawer>     
                <Drawer
                    title={"Build"}
                    placement="right"
                    width={"50%"}
                    closable={true}
                    onClose={this.openBuild}
                    visible={this.state.visibleBuild}
                >
                    <div onClick={this.sendLike(this.state.buildInfo.build_name)}><HeartOutlined/>{this.state.like}</div>
                </Drawer>      
            </div>
        );
    }
}
const mapStateToProps = state => {
    return{
        userData: state.NavbarReducer.user
    }
}

export default connect(mapStateToProps)(Builds);
