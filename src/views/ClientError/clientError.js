import React , {Component} from 'react';
import NotFound from '../../Components/notFound/notFound';
import "./clientError.scss";

class ClientError extends Component{
    render(){
        return(
            <div id="page404">
                <div id="img-404">
                    <NotFound />
                </div>
            </div>
        )
    }
}

export default ClientError;