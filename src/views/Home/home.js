import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import "./home.scss";

// import {}

class Home extends Component {

    constructor(props){
        super(props);
        this.state = {
            collapse : false
        }
    }

    collapse = value => {
        return !value ? this.setState({collapse: true}) : false;
    }

    render(){

        return(
            <div id="home" className={"navbar_" + this.state.collapse}>
                <Link to="/champions">
                    <div id="home-champions" onClick={e => this.collapse(this.state.collapse)}>
                        <div className="homeChampionsImages"> </div>
                        <h2>Champions</h2>
                    </div>
                </Link>
                <Link to="/builds" >
                    <div id="home-builds" >
                        <div className="homeBuildsImages" onClick={e => this.collapse(this.state.collapse)}></div>
                        <h2>Builds</h2>
                    </div>
                </Link>
                <Link to="/live">                
                    <div id="home-live">
                        <div className="homeLiveImages" onClick={e => this.collapse(this.state.collapse)}></div>
                        <h2>Live</h2>
                    </div>
                </Link>
                <Link to="/stream">
                    <div id="home-stream">
                    <div className="homeStreamImages" onClick={e => this.collapse(this.state.collapse)}></div>
                        <h2>Stream</h2>
                    </div>
                </Link>
                <Link to="/patch">
                    <div id="home-patch" >
                        <div className="homePatchImages" onClick={e => this.collapse(this.state.collapse)}></div>
                        <h2>Patch Notes</h2>
                    </div>
                </Link>
            </div>
        );
    }
}

export default Home;