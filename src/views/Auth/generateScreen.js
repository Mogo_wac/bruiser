import Aatrox from '../../assets/auth/aatrox.mp4';
import Darius from '../../assets/auth/darius.mp4';
import Irelia from '../../assets/auth/irelia.mp4';
import Leona from '../../assets/auth/leona.mp4';
import Neeko from '../../assets/auth/neeko.mp4';
import Pyke from '../../assets/auth/pyke.mp4';
import Urgot from '../../assets/auth/urgot.mp4';
import Zed from '../../assets/auth/zed.mp4';
import Zoe from '../../assets/auth/zoe.mp4';

const loginScreen = [
    {src:Aatrox},
    {src:Darius},
    {src:Irelia},
    {src:Leona},
    {src:Neeko},
    {src:Pyke},
    {src:Urgot},
    {src:Zed},
    {src:Zoe}
]


export { loginScreen }