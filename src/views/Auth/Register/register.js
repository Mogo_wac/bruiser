import React, {Component} from 'react';
import SignUp from '../../../Components/auth/signUp'
import  {loginScreen} from '../generateScreen';
import './register.scss';

class Register extends Component {

    constructor(props){
        super(props);
        this.state = {
            screen: {}
        }
    }

    componentDidMount(){
        this.randomScreen(loginScreen);
    }

    randomScreen = (value) => {
        const min = Math.ceil(0);
        const max = Math.ceil(loginScreen.length);
        const random = Math.floor(Math.random() * (max - min)) + min;
        this.setState({screen : loginScreen[random]});
    }

    render(){
        return(
             <div id="register">
             <video autoPlay="true" loop muted id="register-vid">
                 <source
                     src={this.state.screen.src} type="video/mp4"
                 />
             </video>
             <SignUp/>
         </div>
        );
    }
}

export default Register;