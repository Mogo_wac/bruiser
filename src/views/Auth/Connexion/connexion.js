import React, {Component} from 'react';
import Login from '../../../Components/auth/login';
import './connexion.scss';
import  {loginScreen} from '../generateScreen';

class Connexion extends Component {
    constructor(props){
        super(props);
        this.state = {
            screen: {}
        }
    }
    componentDidMount(){
        this.randomScreen(loginScreen);
    }

    randomScreen = (value) => {
        const min = Math.ceil(0);
        const max = Math.ceil(loginScreen.length);
        const random = Math.floor(Math.random() * (max - min)) + min;
        this.setState({screen : loginScreen[random]});
    }

    render(){
        
        return(
            <div id="connexion">
                <video autoPlay="true" loop muted id="login-vid">
                    <source
                        src={this.state.screen.src} type="video/mp4"
                    />
                </video>
                <Login/>
            </div>
        );
    }
}

export default Connexion