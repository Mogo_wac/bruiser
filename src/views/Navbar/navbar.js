import React, {Component} from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import 'antd/dist/antd.css';

import "./navbar.scss";
import background from '../../assets/background/background.mp4';
import NavbarLeft from '../../Components/Navbar/NavbarLeft/navbarLeft';
import NavbarTop from '../../Components/Navbar/NavbarTop/navbarTop';

// import {}

class Navbar extends Component {

    constructor(props){
        super(props);
        this.state = {
        }
    }


    render(){
        
        return(
            <div id="navigation">
                <div id="navbar">
                    <NavbarTop page={this.props.location} />
                </div>
                
                    <video autoplay="true" loop muted id="content-vid" >
                        <source src={background}  />
                    </video>
                <div id="content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Navbar;