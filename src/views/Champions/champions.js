import React, {Component} from 'react';
import './champions.scss';
import socketIo from "socket.io-client";
import Socket, { socket } from "../../Socket/socket";
import ReactTable from 'react-table-6';
import 'react-table-6/react-table.css';
import Galery from "react-photo-gallery";
import {Drawer, Tabs} from 'antd';
import Ad from "../../assets/champView/ad.png";
import Ap from "../../assets/champView/ap.png";
import Shield from "../../assets/champView/shield.png";
import Easy from "../../assets/champView/easy.png";
import Simple from "../../assets/champView/simple.png";
import Medium from "../../assets/champView/medium.png";
import Hard from "../../assets/champView/hard.png";
import Expert from "../../assets/champView/expert.png";

// import Ad from "../../assets/champView/ad.png";

const {TabPane} = Tabs;

class Champions extends Component{
    constructor(props){
        super(props);
        this.state = {
            items: "",
            data : {},
            error: "",
            champions: [],
            champion: [],
            sidebarVisible: false
        }
    }

    componentDidMount(){
        socket.emit("getChampions");
        socket.on("getChampionsResult", res => {
            if (res.response) {
                this.setState({
                    data: res.response.data
                });
            }
            else{
                let error = "Something went wrong during data loading";
                this.setState({error: error});
            }
        })
    }
    onRowClick = (state, rowInfo, column, instance) => {
        
        return {
            onClick: (e, handleOriginal) => {
                console.log(rowInfo.original);
                
                    this.setState({champion: rowInfo.original, sidebarVisible: true})
                }
        }
    }

    closeSidebar = (value) =>{
        return this.setState({sidebarVisible: !value});
    }

    callbackHistogramme = (value, id) => {
        
        let style = {position: "relative", height : `${value}0px`, background: `hsla(240,100%,50%, ${value < 10 ? "."+value : 1})`, width: "30px", margin: "15px", borderRadius: "10px"}  
        let spanStyle = {position: "absolute", top: "-30px", left: "50%", fontSize: "16px",fontWeight: "bold",transform: "translateX(-50%)", opacity: "1", color: `hsla(240,100%,50%, ${value < 10 ? "." + value : 1})`}
        return <div className="histogrammeContainer">
            <div className="" style={style}><span style={spanStyle} className="histogrammeOverlay">{value}</span></div>
        </div>
    }

    render(){

        const columns =[
            {
                Header:"Icones",
                accessor: "image.full",
                filterable: false,
                Cell : props => {
                    return <div style={{width: "100%", textAlign: "center"}}><img src={"//ddragon.leagueoflegends.com/cdn/11.2.1/img/champion/" + props.value} alt={"icone champion"} /></div>
                },
            },
            {
                Header:"Champions",
                filterMethod: (filter, row, column) => {
                    const id = filter.pivotId || filter.id
                    return row[id] !== undefined ? String(row[id].toLowerCase()).startsWith(filter.value.toLowerCase()) : true
                  },
                accessor:"id",
                Filter: ({ filter, onChange }) =>
                    <input placeholder="Recherche" className='text-center inputfilters' style={{ width: "100%", color: "white" }} onChange={event => onChange(event.target.value)} /> ,
                Cell : props =>{
                    
                    return <div style={{width: "100%", textAlign: "center"}}><h2 className="champTitle">{props.value}</h2></div>
                }
            },
            {
                id:"passive",
                Header: "Passif",
                filterable: false,
                accessor: element => {
                    return <div style={{width: "100%", textAlign: "center"}}><img src={"//ddragon.leagueoflegends.com/cdn/11.2.1/img/passive/" + element.passive.image.full} width="64" height="64" /></div>
                }
            },
            {
                id:"spells",
                Header: "Sorts",
                filterable: false,
                accessor: element => element.spells.map((spells, i) => {
                    return <div key={i} className="spellContainer">
                        <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/spell/${spells.image.full}`} style={{margin: '10px', width:"64px", height: "64px"}} />
                        <div className="spellOverlay"><h6>{spells.name}</h6><p>{spells.description}</p></div>
                    </div>
                }),

            },
            {
                id:"info",
                Header: "Difficulté",
                filterable: false,
                accessor: element => {
                    return <div style={{width: "100%", textAlign: "center"}}>
                    {element.info.difficulty <= 2 ?
                    <div>Débutant</div>
                    :
                    element.info.difficulty > 2 && element.info.difficulty <=4 ?
                    <div>Simple</div>
                    :
                    element.info.difficulty > 4 && element.info.difficulty <=6 ?
                    <div>Moyenne</div>
                    :
                    element.info.difficulty > 6 && element.info.difficulty <=8 ?
                    <div>Avancée</div>
                    :
                    element.info.difficulty > 8 ?
                    <div>Expert</div>
                    : ""}
                    </div>
                }

            },
        ];
        let newData = [];
        newData.push(Object.entries(this.state.data));
        let formatData = newData[0].map(element => element[1]);
        
        return (
            <div id="champions">
                <ReactTable
                    className="champ-table"
                    data={formatData.length > 0 ? formatData : []}
                    columns={columns}
                    noDataText={"Something went wrong during data loading."}
                    getTrProps={this.onRowClick}
                    // showPaginationTop={true}
                    // showPaginationBottom={false}
                    minRows={10}
                    filterable={true}
                    defaultPageSize={25}
                />
                <Drawer 
                    title={<div style={{}}><img style={{ height: "64px", width :"64px", margin:"0 20px 0 0"}} src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/champion/${this.state.champion.image ? this.state.champion.image.full : "toto"}`} alt={this.state.champion.id + " picture"} /><span style={{fontSize: "31px", color: "#606060"}}>{this.state.champion.id}</span></div>}  
                    className="champSidebar" 
                    placement="right"
                    closable={true}
                    onClose={(e) => this.closeSidebar(this.state.sidebarVisible)}
                    visible={this.state.sidebarVisible}   
                    width="50%"    
                >
                    <Tabs defaultActiveKey="1" >
                        <TabPane tab="Tips" key="1">
                            <ol style={{listStyleType: "circle"}}>
                                {
                                    this.state.champion.allytips ? 
                                    this.state.champion.allytips.map((element,i) => {
                                        return (
                                            <li key={i} style={{fontSize: '16px', color:"#606060", padding: "20px"}}>{element}</li>
                                        )
                                    })
                                :""
                            }
                            </ol>
                        </TabPane>
                        <TabPane tab="Counter" key="2">
                            <ol style={{listStyleType: "circle"}}>
                                {
                                    this.state.champion.enemytips ? 
                                    this.state.champion.enemytips.map((element,i) => {
                                        return (
                                            <li key={i} style={{fontSize: '16px', color:"#606060", padding: "20px"}}>{element}</li>
                                        )
                                    })
                                :""
                            }
                            </ol>
                        </TabPane>
                        <TabPane tab="Passif" key="3">
                            {
                                this.state.champion.passive ?
                                    <ul style={{listStyleType: "none", color:"#606060"}}>
                                        <li style={{display: "flex", marginBottom: "20px"}}>
                                            <img style={{display: "inline", margin: "0 20px 0 0"}} alt={this.state.champion.id + " passif"} src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/passive/${this.state.champion.passive.image.full}`} width="64" height="64" /><span style={{fontSize: "21px", alignItems:"center", display: "table"}}>{this.state.champion.passive.name}</span>
                                        </li>
                                        <li style={{fontSize: "16px"}}>{this.state.champion.passive.description}</li>
                                    </ul>    
                                : ""
                                }
                        </TabPane>
                        <TabPane tab="Sort" key="4">
                            <ul style={{listStyleType: "none", color:"#606060"}} >
                                {
                                    this.state.champion.spells ? 
                                    this.state.champion.spells.map((element,i) =>{
                                        return(
                                        <li key={i} style={{margin: "20px", padding: "20px 0", borderBottom: "1px solid grey"}}><img style={{margin: "20px"}} alt="champion spells" src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/spell/${element.image.full}`} width="64" height="64" /><span style={{fontSize: "21px", fontWeight:"bold"}}>{element.name}</span>
                                            <div style={{margin: "15px 0"}}>cooldown  par niveau : <span style={{fontWeight: "bold"}}>{element.cooldown.map((cd ,i) => {
                                                return cd + "/"
                                            })}  secondes </span></div>
                                            <div style={{marginBottom: "20px"}}>{element.costBurn === "0" ? "Pas de cout" : "Cout du sort: "}<span style={{fontWeight: "bold"}}>{element.costBurn > "0" ?  element.costBurn : ""}</span></div>
                                            <div style={{fontSize :"16px"}}>{element.description}</div>
                                        </li>
                                        )
                                    })
                                    : ""
                                }
                            </ul>
                        </TabPane>
                        <TabPane tab="Infos" key="5">
                            <div style={{margin: "20px", padding:"20px"}}>
                                <h4 style={{color: "#606060"}}>
                                {
                                    this.state.champion.tags ? 
                                        this.state.champion.tags.map((tags,i)=>{
                                            return <span style={{fontSize: "21px", fontWeight:"bold", paddingRight:"10px"}}>{tags + " " }</span>
                                        })
                                    : ""
                                }
                                </h4>
                                <div style={{display:"flex", alignItems: "baseline", width: "fit-content", margin: "30px 0 100px 0", padding:"20px 0"}}>
                                    <div style={{position: "relative"}}>
                                        {this.state.champion.info ? this.callbackHistogramme(this.state.champion.info.attack) : ""}
                                        <img style={{position: "absolute", bottom: "-30px", left: "50%", transform: "translateX(-50%)"}} src={Ad}  alt="physical dmg icon"/>
                                    </div>
                                    <div style={{position: "relative"}}>
                                        {this.state.champion.info ? this.callbackHistogramme(this.state.champion.info.defense) : ""}
                                        <img style={{position: "absolute", bottom: "-30px", left: "50%", transform: "translateX(-50%)"}} src={Shield} alt="armor and resist icon"/>
                                    </div>
                                    <div style={{position: "relative"}}>
                                        {this.state.champion.info ? this.callbackHistogramme(this.state.champion.info.magic) : ""}
                                        <img style={{position: "absolute", bottom: "-30px", left: "50%", transform: "translateX(-50%)"}} src={Ap} alt="ap icon" />
                                    </div>
                                    <div style={{position: "relative"}}>
                                        {this.state.champion.info ? this.callbackHistogramme(this.state.champion.info.difficulty) : ""}
                                        {
                                            this.state.champion.info ?
                                            <img style={{position: "absolute", bottom: "-30px", left: "50%", transform: "translateX(-50%)"}} src={this.state.champion.info.difficulty > 0 && this.state.champion.info.difficulty <=2 ? Easy : this.state.champion.info.difficulty > 2 && this.state.champion.info.difficulty <= 4 ? Simple : this.state.champion.info.difficulty > 4 && this.state.champion.info.difficulty  <=6 ? Medium : this.state.champion.info.difficulty > 6 && this.state.champion.info.difficulty <= 8 ? Hard : this.state.champion.info.difficulty > 8 ? Expert : ""} alt="Difficulty icon" />
                                            : ""
                                    }
                                    </div>
                                </div>
                                <ul style={{display: "flex", listStyleType: "none", paddingTop: "50px", padding: "0"}}>
                                    <li style={{paddingRight: " 20px"}}><img style={{paddingRight: "20px"}} src={Ad} alt="Ad icon"/><span>Dégats physique</span></li>
                                    <li style={{paddingRight: " 20px"}}><img style={{paddingRight: "20px"}} src={Shield} alt="Shield icon"/><span>Résistances aux dégats</span></li>
                                    <li style={{paddingRight: " 20px"}}><img style={{paddingRight: "20px"}} src={Ap} alt="Ad icon"/><span>Dégats magiques</span></li>
                                    <li ><img style={{paddingRight: "20px"}} src={Easy} alt="Difficulty icon"/><span>Difficulté du champion</span></li>
                                </ul>
                            </div>
                        </TabPane>
                    </Tabs>
                </Drawer>
            </div>
        )
    }
}

export default Champions;