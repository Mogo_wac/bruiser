import React , {Component} from "react";
import { Form, Input, Button} from 'antd';
import "./live.scss";
import Socket, { socket } from "../../Socket/socket";
import ReactTable from "react-table-6";

export default class Live extends Component{
    constructor(props){
        super(props);
        this.state = {
            data: {}
        }
    }

    onFinish = (values) => {
        console.log('Success:', values);
        
        const {summonner} = values;

        socket.emit("getMatch", summonner);
        socket.on("getMatchResult", (res)=>{
            if (!res) {
                console.log("something went wrong");
            }
            return this.setState({data: res.response})
        })
      };
    
    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };

    render(){
        console.log(this.state.data);
        // let dataBlueParticipant = [];
        // let dataBlueParticipantsIds = [];
        // let blueData = [];
        // let redData = [];

        // blueData.push(this.state.data)
        // redData.push(this.state.data)
        // let blueData = [];
        // this.state.data.participants && this.state.data.participantIdentities blueData.push([
        //     {
        //         summonerName : this.state.data.participants.map(element => {
                    
        //             this.state.data.participantIdentities.filter(items => {
        //                 return items.participantId === element.participantId && element.teamId === 100
        //                 // console.log(element.participantId === items.participantId);
                        
        //             }).map(value =>{
        //                 console.log(value);
        //                 element.new_sumName = value.player.summonerName
        //             })
                    
        //             return element.new_sumName ? element.new_sumName : false
                    
        //         })
        //     },
        //     {

        //     }
            
        // ]
        // console.log(blueData);
        
        
        // const formatData = this.state.data.participants && this.state.data.participants.map(element => {
        //     if (element.teamId === 100) {
        //         dataBlue.push(element);
                
        //     }
        // });
        // const formatDataParticipant = this.state.data && this.state.data.participants && this.state.data.participants.map(element => {
        //     if (element.teamId===100) {
        //         dataBlueParticipant.push(element);
        //     }
            
        // });
        // const dataBlueParticipantIds = this.state.data && this.state.data.participantIdentities && this.state.data.participantIdentities.filter(element =>{
        //     dataBlueParticipantsIds.push(dataBlueParticipant.filter(items => items.participantId === element.participantId).map(array => {
        //         return array.length = 0 ? "" : array
        //     }))
            
        // })
        // blueData.push(dataBlueParticipant,dataBlueParticipantsIds)
        // console.log(blueData);
        
        const columnsBlue = [
            {
                id: "participants",
                Header: "Champions",
                // accessor: d => d.participants.map(element =>{
                //     console.log(element);
                    
                // })
                accessor: d => d.participants && d.participants.map(element => {
                    if (element.teamId=== 100) {
                    return <div>{element.championId}</div>
                    }
                })
                
                
                
            },
            {
                id: "participantsIdentities",
                Header: "Invocateur",
                accessor: d => d.participantIdentities && d.participantIdentities.map(element =>{
                    d.participants.filter(items => items.participantId === element.participantId && items.teamId === 100).map(value =>{
                        return element.new_sum = element.player.summonerName;
                    })
                    return <div>{element.new_sum}</div>
                }) 
            },
            {
                id:"stats",
                Header:"KDA",
                accessor: d=> d.participants && d.participants.map(e => {
                return <div>{e.stats.kills} - {e.stats.deaths} - {e.stats.assists}</div>
                    }
                ),
            }
        ]

        const columnsRed = [
            {
                Header: "NAME",
                accessor: "name"
            },
            {
                Header: "AGE",
                accessor: "age"
            }
        ]
        return(
            <div id="direct">
                <Form
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}
                >
                <Form.Item
                    label="Invocateur"
                    name="summonner"
                    rules={[{ required: true, message: 'Please input your summoner id!' }]}
                >
                    <Input placeholder="Nom d'invocateur" />
                </Form.Item>
                <Form.Item >
                    <Button type="primary" htmlType="submit">
                        Chercher
                    </Button>
                </Form.Item>
                </Form>
                <div id="match" style={{display: "flex"}}>
                    {/* <div
                        id="blue-team"
                    >
                        Equipe bleu
                        <ReactTable
                            className="blue-team"
                            data={blueData}
                            columns={columnsBlue}
                            defaultPageSize={10}
                            showPagination={false}
                        />
                    </div>
                    <div
                        id="red-team"
                    >
                        Equipe Rouge
                        <ReactTable
                            className="red-team"
                            data ={redData}
                            columns={columnsRed}
                            defaultPageSize={10}
                            showPagination={false}
                        />
                    </div> */}
                </div>
            </div>
        );
    }
}