import Socket, { socket } from '../socket';

class User {
    static getProps(props) {
        this.props = props;
    }
    static loginUser(user) {
        return new Promise((resolve, reject) => {
            socket.on("login", res => {
            })
            Socket.login(user)
            .then(res => {
                if (!res) {
                    reject(res);
                }
                resolve(res);
            })
            .catch(err => {
                reject(err);
            });
        })
    }

    static registerUser(user){
        return new Promise((resolve, reject) => {
            Socket.register(user)
            .then(res => {
                console.log("USER RES" + res);
                
                if (!res) {
                    reject(res);
                }
                resolve(res);
            })
            .catch(err => {
                console.log("USERS ERR : " + err);
                
                reject(err);
            })
        });
    }

    static addUsername(username, email){
        return new Promise((resolve,reject) => {
            const userData = {username, email}
            Socket.addUsername(userData)
            .then(res => {
                resolve(res);
            })
            .catch(err => {
                reject(err);
            })
        })
    }

    static updatePassword(actualPassword, newPassword,email){
        return new Promise((resolve, reject)=> {
            const userPass = {actualPassword, newPassword, email}
            Socket.changePassword(userPass)
            .then(res => {
                !res ? reject(res): resolve(res);
            })
            .catch(err => {
                reject(err);
            })
        })
    }
    static getUsers(entityId){
        console.log(entityId);
        return new Promise((resolve,reject) =>{
            Socket.getUsers(entityId)
            .then(res =>{
                console.log(res);
                res ? resolve(res) : reject(res);
            })
            .catch(err =>{
                reject(err);
            })
        })
        
    }
    static deleteUser(email){
        console.log("email:", email);
        return new Promise((resolve,reject) =>{
            Socket.deleteUser(email)
            .then(res =>{
                console.log(res);
                res ? resolve(res) : reject(res);
            })
            .catch(err =>{
                reject(err);
            })
        })
    }

    static changeAvatar(img){
        console.log(img);
        return new Promise((resolve, reject)=>{
            Socket.updateAvatar(img)
            .then(res =>{
                res ? resolve(res) : reject(res)
            })
            .catch(err =>{
                reject(err)
            })
        })
        
    }
}

export default User;