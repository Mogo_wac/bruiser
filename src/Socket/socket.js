import io from "socket.io-client";
export const socket = io("http://localhost:8000");

class Socket {
    static getProps(props) {
        this.props = props;
      }
    static login(user){
        
        return new Promise((resolve, reject) => {
            socket.emit("login", user)
                socket.on("loginResult", res => {
                    if (res.token) {
                        resolve(res);
                    }       
                    else{
                        reject(res.error);
                    }
                })

        })
    }

    static register(user){
        return new Promise((resolve,reject) => {
            socket.emit("register", user)
            socket.on("registerResult", res => {
                if (res.register) {
                    resolve(res)
                }
                else{
                    reject(res.error);
                }
            })
        })
    }

    static search(data){
        return new Promise((resolve, reject) => {
            socket.emit("search", data);
            socket.on("searchResult", (res)=>{
                console.log(res);
                
            })
        })
    }

    static addUsername(userData){
        console.log(userData);
        return new Promise((resolve, reject) => {
            socket.emit("addUsername", userData);
            socket.on("addUsernameResult", res => {
                if (res) {
                    resolve(res);
                }
                else{
                    reject(res);
                }
            })
        })    
    }

    static changePassword(userPass){
        return new Promise((resolve,reject) => {
            socket.emit("updatePass", userPass);
            socket.on("updatePassResult", res => {
                if (res.response) {
                    console.log("SOCKET OK", res);
                    
                    resolve(res);
                }
                else{
                    console.log("SOCKET ERR", res);
                    
                    reject(res);
                }
            })
        })        
        
    }

    static getUsers(entityId){
        
        return new Promise((resolve, reject) =>{
            socket.emit("getUsers", entityId);
            socket.on("getUsersResult", res =>{
                if (res.response) {
                    resolve(res);
                }
                else{
                    reject(res);
                }
            })
        })
    }

    static deleteUser(email){
        console.log(email);
        
        return new Promise((resolve, reject) =>{
            socket.emit("deleteUser", email);
            socket.on("deleteUserResult", res =>{
                if (res.response) {
                    console.log("SOCKET OK", res);
                    
                    resolve(res);
                }
                else{
                    console.log("SOCKET ERR", res);
                    
                    reject(res);
                }
            })
        })
    }
    static updateAvatar(img){
        console.log(("socket: ", img));
        return new Promise((resolve, reject) =>{
            socket.emit("updateAvatar", img);
            socket.on("updateAvatarResult", res =>{
                if (res.response) {
                    console.log("SOCKET OK", res);
                    
                    resolve(res);
                }
                else{
                    console.log("SOCKET ERR", res);
                    
                    reject(res);
                }
            })
        })
    }
}

export default Socket;