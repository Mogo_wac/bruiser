import React, {Component} from 'react';
import './App.css';
import 'antd/dist/antd.css';
import {
  Switch,
  Route,
  Router
} from "react-router";
import { createBrowserHistory } from 'history/';
import Connexion from './views/Auth/Connexion/connexion';
import Register from './views/Auth/Register/register';
import Home from './views/Home/home';
import Navbar from './views/Navbar/navbar';
import {routes} from './router/routes';

const browserHistory = createBrowserHistory();

class App extends Component{

  render(){
    return(
      <div id="bruiser">
        <Router history={browserHistory}>
          <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/login" component={Connexion} />
          <Route exact path="/register" component={Register} />
            <Navbar data="toto">
              <Switch>
                {
                  routes.map((element,i) => {
                    return <Route path={element.path} component={element.component} key={i} />
                  })
                }
                </Switch>
              </Navbar>
          </Switch>
        </Router>
      </div>
    );
  }
}



export default App;
