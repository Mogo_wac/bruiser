import React, {Component} from 'react';
import './navbarTop.scss';
import { Avatar, Dropdown, Menu, Drawer, Tabs, Input, Space , Button, Image,Select, Form } from 'antd';
import { UserOutlined, LockOutlined, SmileOutlined, MehOutlined } from '@ant-design/icons';
import {Link, Redirect} from 'react-router-dom';
import Logo from "../../../assets/logo/logo-white.png";
import Socket, { socket } from '../../../Socket/socket';
import {connect} from 'react-redux';
import User from '../../../Socket/User/user';

const { Search } = Input;
const { TabPane } = Tabs;
const { Option } = Select;

class NavbarTop extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoaded: false,
            items: [],
            token: "",
            visible: false,
            userEmail: "",
            response :"",
            ban : [],
            status: [],
            champions: [],
            img: "",
            avatar: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
        }
    }
    componentDidMount(){

        const token = window.localStorage.getItem("x-access-token");
        
        if (token !== "" && token !== null) {
            this.setState({token: token});
            
        }

        if (this.props.userData && this.props.userData.entity_id === 1) {
            User.getUsers(this.props.userData.entity_id)
            .then(res => {
                console.log("getUsers res:", res.response);
                return this.setState({ban: res.response});
            })
            .catch(err => {
                console.log("failed getting users:", err);
                
            })
            ;
        }

        socket.emit("getStatus");
        socket.on("getStatusResult", (res)=>{
            return this.setState({status: res.status})
        });
        socket.emit("getChampions");
        socket.on("getChampionsResult", res => {
            if (res.response) {
                this.setState({
                    champions: res.response.data
                });
            }
            else{
                let error = "Something went wrong during data loading";
                this.setState({error: error});
            }
        })
 
    }
    search = (e) => {
        Socket.search(e.target.value);
    }

    disconnect = () => {
        window.localStorage.clear();
    }

    showDrawer = (value) => {
        console.log("visible");
        
        this.setState({visible: !value})
      };
    onClose = () => {
        console.log("not visible");
        
        this.setState({visible: false});
    };

    menu = () => {
        console.log("execute") ;
     return(
        <Menu>
          <Menu.Item>
            <span onClick={() => this.showDrawer(this.state.visible)}>
                Profile
            </span>
          </Menu.Item>
          <Menu.Item >
            <Link to="/login" onClick={() => {window.localStorage.clear()}}>Déconnexion</Link>
          </Menu.Item>
        </Menu>
    )};

    callback(key) {
    }

    onFinish = (values) => {
        const{actualPassword, newPassword, checkNewPassword} = values;
        const email = this.props.userData ? this.props.userData.email : "";
        if (newPassword !== checkNewPassword) {
            console.log("pb password");
            
            this.setState({response : "Error during password verification please verify each input."});
            let output = document.getElementById("tabs-output-password");
            output.style.opacity = 1;
            setTimeout(function(){
                output.style.opacity = 0;
            }, 3500);
            return false;

        }
        else if(email === ""){
            this.setState({response : "Error please re-login."});
            let output = document.getElementById("tabs-output-password");
            output.style.opacity = 1;
            setTimeout(function(){
                output.style.opacity = 0;
            }, 3500);
            return false;
        }

        User.updatePassword(actualPassword, newPassword,email)
        .then(res => {
                this.setState({response: res.response});
                let output = document.getElementById("tabs-output-password");
                output.style.opacity = 1;
                output.style.color = "lightgreen";
                setTimeout(function(){
                    output.style.opacity = 0;
                }, 3500);
        })
        .catch(err => {
            console.log("err: " + err);
            this.setState({response: err.error});
            let output = document.getElementById("tabs-output-password");
            output.style.color = "red";
            output.style.opacity = 1;
            setTimeout(function(){
                output.style.opacity = 0;
            }, 3500);
        })
    };
    
    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    addUsername = (value) => {
        const {username} = value;
        const email = this.props.userData.email;
        
        User.addUsername(username, email)
        .then(res => {
            this.setState({response: res.username});
            let output = document.getElementById("tabs-output");
            output.style.opacity = 1;
            setTimeout(function(){
                output.style.opacity = 0
            }, 3500);
            console.log(res);
        })
        .catch(err => {
            console.log(err);
        })
        
    }

    addUsernameFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    }

    banUsers = (value) => {
        console.log("on passe ici:",value);
        User.deleteUser(value)
        .then(res =>{
            console.log("user deleted: ", res);
            this.setState({response : res.response});
            let output = document.getElementById("tabs-output-ban");
            output.style.opacity = 1;
            setTimeout(function(){
                output.style.opacity = 0
            }, 3500);
        })
        .catch(err =>{
            console.log("failed deleting user: ", err);
        })
    }

    changeImg = value => {
        console.log(value);
        
        if(!this.props.userData){
            this.setState({response : "Veuillez vous connecter"});
            let output = document.getElementById("tabs-output-avatar");
            output.style.opacity = 1;
            output.style.color = "red"
            setTimeout(function(){
                output.style.opacity = 0;
            }, 3500);
            return false;
        }
        const email = this.props.userData.email;
        const data = {value, email}
        console.log(data);
        User.changeAvatar(data)
        .then(res =>{
            console.log("res avatar :" + res);
        })
        .catch(err =>{
            console.error(err);
            
        })
        return this.setState({avatar: value});
        
    }

    render(){

        let newData = [];
        newData.push(Object.entries(this.state.champions));
        let formatData = newData[0].map(element => element[1]);

        console.log(this.props);
        
        
        return(
            <div id="navbar-top">
                <div id="navbar-logo">
                    <Link to="/">
                        <img src={Logo} alt="Bruiser Logo" id="logo"/>
                    </Link>
                </div>
                <div id="search-bar">
                    <Space >
                        <Search onChange={this.search} placeholder="Recherche" />
                    </Space>
                </div>
                <div id="navbar-links">
                    <ul className="navbar-items">
                        <li className={this.props.page.pathname === "/champions" ? "active-link": ""}>
                            <Link to="/champions">
                                Champions
                            </Link>
                        </li>
                        <li className={this.props.page.pathname === "/builds" ? "active-link" : ""}>
                            <Link to="/builds">
                                Builds
                            </Link>
                        </li>
                        <li className={this.props.page.pathname === "/live" ? "active-link" : ""}>
                            <Link to="/live">
                                Live
                            </Link>
                        </li>
                        <li className={this.props.page.pathname === "/stream" ? "active-link" : ""}>
                            <Link to="/stream">
                                Stream
                            </Link>
                        </li>
                        {/* <li className={this.props.page.pathname === "/patch" ? "active-link" : ""}>
                            <Link to="/patch">
                                Patch Notes
                            </Link>
                        </li> */}
                    </ul>
                </div>
                <div id="navbar-avatar">
                        {
                            !this.state.token  ?
                                <ul className="navbar-items">
                                    <li>
                                        <Link to="/login">
                                            <Button
                                                id="navbar-login"
                                            >
                                                Login
                                            </Button>
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="/register">
                                            <Button
                                                id="navbar-register"
                                                >
                                                Sign up
                                            </Button>
                                        </Link>
                                    </li>
                                </ul>
                            :
                             false
                        }
                        {
                            this.state.token && this.state.token !== "" ?
                                <ul className="navbar-items">
                                    <li>
                                        <div id="avatar-container">
                                            <Dropdown id="avatar-dropdown" overlay={this.menu} placement="bottomCenter" >
                                            <Avatar
                                                id="avatar"
                                                icon={this.props.userData && this.props.userData.avatar !== "" ? <img src={`${this.props.userData.avatar}`} /> : <UserOutlined/>  }
                                            />
                                            </Dropdown>
                                        </div>
                                    </li>
                                </ul>
                            :
                            false
                        }
                    {/* </ul> */}

                </div>
                <Drawer
                    title={"Profile " + ( this.props.userData ? this.props.userData.username : "" )}
                    placement="right"
                    width={"33%"}
                    closable={true}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    id="navbar-sidebar"
                >
                    <Tabs defaultActiveKey="1" onChange={this.callback}>
                        <TabPane tab={<span><UserOutlined/>Invocateur</span>} key="1">
                            <h3>Ajouté votre nom d'invocateur !</h3>
                            <Form
                                name="basic"
                                initialValues={{ remember: true }}
                                onFinish={this.addUsername}
                                onFinishFailed={this.addUsernameFailed}
                            >
                                <Form.Item
                                    label="Nom d'invocateur"
                                    name="username"
                                    rules={[{ required: true, message: "Veuillez insérer votre nom d'invocateur!" }]}
                                >
                                    <Input placeholder={"Nom d'invocateur"} />
                                </Form.Item>
                                <Form.Item>
                                    <Button type="primary" htmlType="submit">
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                            <div id="tabs-output">{this.state.response}</div>
                        </TabPane>
                        <TabPane tab={<span><SmileOutlined/>Avatar</span>} key="2">
                            <h3>Choisissez votre champions préféré comme avatar !</h3>
                            <Image
                                width={100}
                                height={100}
                                src={this.state.avatar}
                                // fallback={this.state.avatar}
                            />
                            <Select 
                                placeholder="Champions" 
                                style={{width: "200px"}} 
                                bordered={false}
                                onChange={this.changeImg}
                            >
                                {
                                    formatData.map((element,i) => {
                                            return <Option key={i} value={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/champion/${element.image.full}`}>
                                                        <img src={`//ddragon.leagueoflegends.com/cdn/11.2.1/img/champion/${element.image.full}`} width="32px" height="32px" style={{margin: "0 15px 0 0"}}/>
                                                        {element.name}
                                                    </Option>
                                        })
                                }
                            </Select>
                            <div id="tabs-output-avatar">{this.state.response}</div>
                        </TabPane>
                        <TabPane tab={<span><LockOutlined/>Password</span>} key="3">
                            <Form
                                name="basic"
                                initialValues={{ remember: true }}
                                onFinish={this.onFinish}
                                onFinishFailed={this.onFinishFailed}
                                id="password-sidebar"
                                >
                                <Form.Item
                                    label="Mot de passe actuel"
                                    name="actualPassword"
                                    rules={[{ required: true, message: "Veuillez insérer votre mot de passe actuel!" }]}
                                >
                                    <Input.Password placeholder="Mot de passe actuel"/>
                                </Form.Item>
                                <Form.Item
                                    label="Nouveau mot de passe"
                                    name="newPassword"
                                    rules={[{ required: true, message: "Veuillez insérer votre nouveau mot de passe!" }]}
                                >
                                    <Input.Password placeholder="Nouveau mot de passe"/>
                                </Form.Item>
                                <Form.Item
                                    label="Vérification mot de passe"
                                    name="checkNewPassword"
                                    rules={[{ required: true, message: 'Veuillez ré-insérer votre nouveau mot de passe!' }]}
                                >
                                    <Input.Password placeholder="Vérification mot de passe" />
                                </Form.Item>
                                <Form.Item >
                                    <Button type="primary" htmlType="submit">
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                                <div id="tabs-output-password" >{this.state.response}</div>
                        </TabPane>
                        <TabPane tab={"Status"} key="4">
                            <ul style={{margin: "20px 0", listStyleType:"none" ,fontSize:"16px"}}>
                            {
                                this.state.status && this.state.status.length > 0 ? this.state.status.map((element,i) =>{
                                    return(
                                        <li key={i} style={{padding:"0 0 15px 10px", display:"flex", alignItems:"center"}}>
                                            <div style={{margin: "0 10px 0 0", width: "10px", height: "10px", borderRadius:"50%", background: `${element.status === "online" ? "lightgreen" : "red"}`}} key={i}></div><span>{element.name + "  -  " + element.status}</span>
                                        </li>
                                    )
                                })
                                :"HIHI"
                            }
                            </ul>
                        </TabPane>
                        {
                            this.props.userData && this.props.userData.entity_id === 1 ?
                            <TabPane tab={<span><MehOutlined/>Ban</span>} key="5">
                                <Select 
                                    placeholder="Users to ban"
                                    onChange={this.banUsers}
                                    bordered={false}
                                    style={{ width: 200 }}
                                >
                                    {
                                        this.state.ban.length > 0 ? this.state.ban.map((element, i) =>{
                                            return(
                                            <Option key={i} value={element}>{element}</Option>
                                            )                       
                                        })
                                        :""
                                    }
                                </Select>
                                <div id="tabs-output-ban">{this.state.response}</div>
                            </TabPane>
                            : false
                        }
                    </Tabs>
                </Drawer>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return{
        userData: state.NavbarReducer.user
    }
}

export default connect(mapStateToProps)(NavbarTop);