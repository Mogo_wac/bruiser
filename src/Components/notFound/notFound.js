import React, {Component} from 'react';
import "./notFound.scss";
import {Button} from 'antd'
import {Link} from 'react-router-dom'

class notFound extends Component{
    render(){
        return(
            <div id="not-found">
                <Link to="/">
                    <Button id="home-btn">
                        Go back home
                    </Button>
                </Link>
            </div>
        )
    }
}

export default notFound;