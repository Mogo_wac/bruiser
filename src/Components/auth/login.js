import React, {Component} from 'react';
import { Form, Input, Button,} from 'antd';
import axios from 'axios';
import './css/login.scss';
import {
    Link, Redirect
} from "react-router-dom";
import Logo from '../../assets/logo/logo-white.png';
import User from "../../Socket/User/user";
import { 
    LockOutlined,
    UserOutlined
 } from '@ant-design/icons';
import {getUser} from "../../redux/action/loginAction";
import { connect } from "react-redux";

const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 4, span: 8 },
  };

  const onFinishFailed = (errorInfo) => {
      console.log('Failed:', errorInfo);
    };
    
class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            response : "",
            redirect : false,
        }
    }

    onFinish = (value) => {
        const {email , password} = value;
        const user = {email, password}
        User.loginUser(user)
        .then(res => {
            if (res.token && res.token !== "") {
                window.localStorage.setItem("x-access-token",JSON.stringify(res.token));
                if (res.user.length > 0) {
                    const userData = {
                        email: res.user[0].email,
                        entity_id: res.user[0].entity_id,
                        entity_name: res.user[0].entity_name,
                        username: res.user[0].username,
                        avatar: res.user[0].avatar
                    }
                    
                    this.props.userData(userData);
                }
                var getToken = window.sessionStorage.getItem("x-access-token");
                return this.setState({redirect: true})
            }
        })
        .catch(err => {
            this.setState({response: err});
            let output = document.getElementById("error-ouput-login");
            output.style.right = "-200%";
            output.style.opacity = "1";
            setTimeout(function (){
                output.style.right = "-250%";
                output.style.opacity = "0";
            }, 3500)
            console.log(err);
            
        })
        ;
        
    };
    onFinishFailed = (errorInfo) => {
        return false;        
    }

    render(){
        
        return(
            !this.state.redirect ? 
                    <div id="login">
                        <div id="error-ouput-login">{this.state.response}</div>
                        <Link to="/">
                            <img 
                                id="logo-login"
                                src={Logo}
                                alt="bruiser logo"
                            />
                        </Link>
                        <h1 id="login-title">Login</h1>
                        <Form
                            id="login-form"
                            {...layout}
                            name="basic"
                            initialValues={{ remember: true }}
                            onFinish={this.onFinish}
                            onFinishFailed={onFinishFailed}
                            >
                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[{ required: true, message: 'Please input your email!' }]}
                            >
                                <Input className="loginInput" placeholder="Email" prefix={<UserOutlined/>} />
                            </Form.Item>
        
                            <Form.Item
                                label="Password"
                                name="password"
                                rules={[{ required: true, message: 'Please input your password!' }]}
                            >
                                <Input.Password className="loginInput" placeholder="Password" prefix={<LockOutlined/>} />
                            </Form.Item>
        
                            <Form.Item >
                                <Button type="primary" htmlType="submit" id="login-submit" onClick={this.callApi}>
                                Submit
                                </Button>
                            </Form.Item>
                        </Form>
                        <div id="register-section">
                            <Link to="/register">
                                <small>Still a noob ?</small><br />
                                <Button id="register-btn" >
                                    Register
                                </Button>
                            </Link>
                        </div>
        
                    </div>

            :
            <Redirect to="/champions" />
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        userData : data => dispatch(getUser(data))
    }
}

export default connect(null, mapDispatchToProps)(Login);

// export default Login