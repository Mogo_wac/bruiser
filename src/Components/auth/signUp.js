import React, {Component} from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import './css/signUp.scss';
import {
    Link,
    Redirect
} from "react-router-dom";
import Logo from '../../assets/logo/logo-white.png';
import User from "../../Socket/User/user";
import { Socket } from 'socket.io-client';
import { 
    LockOutlined,
    UserOutlined
 } from '@ant-design/icons';


const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 4, span: 8 },
  };



class SignUp extends Component {

    constructor(props){
        super(props);
        this.state = {
            response : "",
            redirect : false,
        }
    }

    onFinish = (value) => {
  
        // verification mdp
      
    //   if (value.password !== value.verifypassword) {
    //       return false;
    //   }
      const {email, password, verifypassword} = value;
      const user= {email,password, verifypassword};
      User.registerUser(user)
      .then(res => {
          console.log("response res: " + res.register);
          this.setState({response : "Succesfully registered, redirecting to Login"})
          let output = document.getElementById("error-output-register");
            output.style.color = "green !important";
            output.style.right = "-200%";
            output.style.opacity = "1";
            this.setState({redirect: true});
            setTimeout(function (){
                output.style.right = "-250%";
                output.style.opacity = "0";
            }, 3500);

      })
      .catch(err => {
          this.setState({response: err});
          let output = document.getElementById("error-output-register");
            output.style.right = "-200%";
            output.style.opacity = "1";
            setTimeout(function (){
                output.style.right = "-250%";
                output.style.opacity = "0";
            }, 3500)
      });
    };
    
    onFinishFailed = (errorInfo) => {
      console.log('Failed:', errorInfo);
    };

    render(){
        return(
            !this.state.redirect ? 
            <div id="sign-up">
                <div id="error-output-register">{this.state.response}</div>
                <Link to="/">
                    <img 
                        id="logo-register"
                        src={Logo}
                        alt="bruiser logo"
                    />
                </Link>
                <h1 id="register-title">Register</h1>
                <Form
                    id="register-form"
                    {...layout}
                    name="basic"
                    initialValues={{ remember: true }}
                    onFinish={this.onFinish}
                    onFinishFailed={this.onFinishFailed}
                    >
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[{ required: true, message: 'Please input your email!' }]}
                    >
                        <Input className="registerInput" placeholder="Email" prefix={<UserOutlined/>}/>
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[{ required: true, message: 'Please input your password!' }]}
                    >

                        <Input.Password className="registerInput" placeholder="Password" prefix={<LockOutlined/>} />
                    </Form.Item>

                    <Form.Item
                        label="verifyPassword"
                        name="verifypassword"
                        rules={[{ required: true, message: 'Please verify your password!' }]}
                    >
                        <Input.Password className="registerInput" placeholder="Verify password" prefix={<LockOutlined/>} />
                    </Form.Item>
                    <div id="password-require">
                        <small >
                            Password requires at least 8 characters.
                        </small>
                    </div>
                    <Form.Item >
                        <Button type="primary" htmlType="submit" id="register-submit" onClick={this.callApi}>
                        Submit
                        </Button>
                    </Form.Item>
                </Form>
                <div id="register-section">
                    <Link to="/login">
                        <small>Already a pro gamer ;) ?</small><br />
                        <Button id="register-btn" >
                            Login
                        </Button>
                    </Link>
                </div>

            </div>
            :
            <Redirect to="/login" />
        );
    }
}

export default SignUp;